import React from 'react';
import ComponentCreator from '@docusaurus/ComponentCreator';

export default [
  {
    path: '/getstarted/__docusaurus/debug',
    component: ComponentCreator('/getstarted/__docusaurus/debug', 'eef'),
    exact: true
  },
  {
    path: '/getstarted/__docusaurus/debug/config',
    component: ComponentCreator('/getstarted/__docusaurus/debug/config', '8fd'),
    exact: true
  },
  {
    path: '/getstarted/__docusaurus/debug/content',
    component: ComponentCreator('/getstarted/__docusaurus/debug/content', 'eda'),
    exact: true
  },
  {
    path: '/getstarted/__docusaurus/debug/globalData',
    component: ComponentCreator('/getstarted/__docusaurus/debug/globalData', '587'),
    exact: true
  },
  {
    path: '/getstarted/__docusaurus/debug/metadata',
    component: ComponentCreator('/getstarted/__docusaurus/debug/metadata', 'b38'),
    exact: true
  },
  {
    path: '/getstarted/__docusaurus/debug/registry',
    component: ComponentCreator('/getstarted/__docusaurus/debug/registry', '518'),
    exact: true
  },
  {
    path: '/getstarted/__docusaurus/debug/routes',
    component: ComponentCreator('/getstarted/__docusaurus/debug/routes', '625'),
    exact: true
  },
  {
    path: '/getstarted/blog',
    component: ComponentCreator('/getstarted/blog', '30b'),
    exact: true
  },
  {
    path: '/getstarted/blog/archive',
    component: ComponentCreator('/getstarted/blog/archive', 'e04'),
    exact: true
  },
  {
    path: '/getstarted/blog/first-blog-post',
    component: ComponentCreator('/getstarted/blog/first-blog-post', '0eb'),
    exact: true
  },
  {
    path: '/getstarted/blog/long-blog-post',
    component: ComponentCreator('/getstarted/blog/long-blog-post', '999'),
    exact: true
  },
  {
    path: '/getstarted/blog/mdx-blog-post',
    component: ComponentCreator('/getstarted/blog/mdx-blog-post', 'b38'),
    exact: true
  },
  {
    path: '/getstarted/blog/tags',
    component: ComponentCreator('/getstarted/blog/tags', 'f3b'),
    exact: true
  },
  {
    path: '/getstarted/blog/tags/docusaurus',
    component: ComponentCreator('/getstarted/blog/tags/docusaurus', '5c3'),
    exact: true
  },
  {
    path: '/getstarted/blog/tags/facebook',
    component: ComponentCreator('/getstarted/blog/tags/facebook', '260'),
    exact: true
  },
  {
    path: '/getstarted/blog/tags/hello',
    component: ComponentCreator('/getstarted/blog/tags/hello', '34a'),
    exact: true
  },
  {
    path: '/getstarted/blog/tags/hola',
    component: ComponentCreator('/getstarted/blog/tags/hola', '773'),
    exact: true
  },
  {
    path: '/getstarted/blog/welcome',
    component: ComponentCreator('/getstarted/blog/welcome', '720'),
    exact: true
  },
  {
    path: '/getstarted/markdown-page',
    component: ComponentCreator('/getstarted/markdown-page', '98b'),
    exact: true
  },
  {
    path: '/getstarted/docs',
    component: ComponentCreator('/getstarted/docs', '918'),
    routes: [
      {
        path: '/getstarted/docs',
        component: ComponentCreator('/getstarted/docs', 'de5'),
        routes: [
          {
            path: '/getstarted/docs',
            component: ComponentCreator('/getstarted/docs', '9de'),
            routes: [
              {
                path: '/getstarted/docs/category/tutorial---basics',
                component: ComponentCreator('/getstarted/docs/category/tutorial---basics', '5ac'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/getstarted/docs/category/tutorial---extras',
                component: ComponentCreator('/getstarted/docs/category/tutorial---extras', '7b2'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/getstarted/docs/intro',
                component: ComponentCreator('/getstarted/docs/intro', '719'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/getstarted/docs/tutorial-basics/congratulations',
                component: ComponentCreator('/getstarted/docs/tutorial-basics/congratulations', '2c8'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/getstarted/docs/tutorial-basics/create-a-blog-post',
                component: ComponentCreator('/getstarted/docs/tutorial-basics/create-a-blog-post', '43c'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/getstarted/docs/tutorial-basics/create-a-document',
                component: ComponentCreator('/getstarted/docs/tutorial-basics/create-a-document', '7e6'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/getstarted/docs/tutorial-basics/create-a-page',
                component: ComponentCreator('/getstarted/docs/tutorial-basics/create-a-page', '5f0'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/getstarted/docs/tutorial-basics/deploy-your-site',
                component: ComponentCreator('/getstarted/docs/tutorial-basics/deploy-your-site', '35b'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/getstarted/docs/tutorial-basics/markdown-features',
                component: ComponentCreator('/getstarted/docs/tutorial-basics/markdown-features', '456'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/getstarted/docs/tutorial-extras/manage-docs-versions',
                component: ComponentCreator('/getstarted/docs/tutorial-extras/manage-docs-versions', '859'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/getstarted/docs/tutorial-extras/translate-your-site',
                component: ComponentCreator('/getstarted/docs/tutorial-extras/translate-your-site', 'b54'),
                exact: true,
                sidebar: "tutorialSidebar"
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: '/getstarted/',
    component: ComponentCreator('/getstarted/', '997'),
    exact: true
  },
  {
    path: '*',
    component: ComponentCreator('*'),
  },
];
